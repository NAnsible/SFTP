SFTP
====

A role to install SFTP

Requirements
------------

- A working debian distribution
- whois package installed on your local host (will be installed if not)

Role Variables
--------------

|Name|Type|Description|Default value|
|----|----|-----------|-------------|
|`SFTPPort`|Integer|The listening port for the openssh server|`22`|
|`SFTPGroupName`|String|The group to create for sftp user|`sftp`|
|`SFTPUserName`|String|The sftp username|`sftp`|
|`SFTPPassword`|String|The sftp password|`test1234`|

Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- hosts: localhost
  roles:
    - SFTP
```


License
-------

Lucas

Author Information
------------------

SOARES Lucas <lucas.soares.npro@gmail.com>

